#!/usr/bin/python3


'''
moodle2scenari -- shortdesc

moodle2scenari converts moodle2scenari in moodle XML files to Scenari quiz (opale, topoze / faq2sciences)

@author:     Stéphane Poinsart
@copyright:  Université de Technologie de Compiègne, Cellule d'Appui Pédagogique
@license:    GPL/LGPL/CECL/MPL
@contact:    stephane.poinsart@utc.fr
'''

import sys
import os
import shutil
import argparse
import unicodedata
import re
import traceback

from lxml import etree

import lxml.html
import lxml.html.soupparser

from lxml.builder import ElementMaker
from macpath import dirname

import urllib
import base64

import html

import yaml
import pprint
import unicodedata

pp = pprint.PrettyPrinter(indent=4)


#import pydevd
#pydevd.settrace("localhost", port=5678)



NSmaps={
    'xml': 'http://www.w3.org/XML/1998/namespace',
    'sc' : 'http://www.utc.fr/ics/scenari/v3/core',
    'op' : 'utc.fr:ics/opale3',
    'sp' : 'http://www.utc.fr/ics/scenari/v3/primitive'
}

from enum import Enum
class contentlvl(Enum):
    FLOWALL = 1
    MULTIPARA = 2
    MULTIPARACLOZE = 3
    PARA = 4

IMAGEDIR="images"


XMLsc = ElementMaker(namespace=NSmaps['sc'], nsmap={'sc': NSmaps['sc']})
XMLop = ElementMaker(namespace=NSmaps.get('op'), nsmap={'op': NSmaps.get('op')})
XMLsp = ElementMaker(namespace=NSmaps.get('sp'), nsmap={'sp': NSmaps.get('sp')})
XMLopsp = ElementMaker(namespace=NSmaps.get('op'), nsmap={'op': NSmaps.get('op'), 'sp': NSmaps.get('sp')})


maincall = argparse.ArgumentParser(description='Convert from Mooodle XML to topoze / opale')
maincall.add_argument('inputfile', help='input moodle XML file (.xml)')
maincall.add_argument('outputdir', help='output directory')
maincall.add_argument('prefix', help='(optionnal) prefix for generated quizz name', nargs='?', default=None)
options = maincall.parse_args()

parser = etree.XMLParser(remove_blank_text=True, remove_comments=False, recover=True)

if (not os.path.isfile(options.inputfile)):
    sys.stderr.write('Error: "'+options.inputfile+'" inputfile is not a file\n')
    sys.exit(1)
inputfile = os.path.realpath(options.inputfile)

if (not os.path.isdir(options.outputdir)):
    os.makedirs(options.outputdir)
    if (not os.path.isdir(options.outputdir)):
        sys.stderr.write('Error: can\'t create output directory "'+options.outputdir+'"\n')
        sys.exit(1)
outputdir = os.path.realpath(options.outputdir)


# question id counter (to ensure uniqueness)
qcount=0
imgcount=0
xmlcount=0

debugtitle=""
themeindex=None

def useless(str):
    return (str is None or str=='' or str.isspace())
    

def errormessage(message):
    if (debugtitle is not None):
        print("Question "+str(xmlcount)+" \""+debugtitle+"\": "+message)
    else:
        print("Question "+str(xmlcount)+" non chargée: "+message)


def nstag(prefix, tag):
    return "{"+NSmaps[prefix]+"}"+tag;



# Create an ID without special char. Count has to be incremented outside of the function.
def generateId(count, text):
    if (not text):
        text='Sans Titre'
    s = ''.join(c for c in unicodedata.normalize('NFD', text) if unicodedata.category(c) != 'Mn')
    s = re.sub('[^0-9a-zA-Z_]+', '-', s.strip())
    s = str(count).zfill(3) + '-' + s
    return s


def cleantheme(text):
    text=''.join((c for c in unicodedata.normalize('NFD', text) if unicodedata.category(c) != 'Mn'))
    text=''.join(e for e in text  if (e.isalnum() or ord(e)==ord(' ')))
    return text.strip().lower()


# load theme file from topoze source code and return a theme index
# topoze/sources/topoze/gen/mirage/svc/svcDistrib.doss/quizConfig.js
# it's javascript and not pure json, but we can manage around that
def loadthemes():
    themeindex=None
    themestr=None
    quizconfigpath=os.path.join(os.path.dirname(os.path.realpath(__file__)), "quizConfig.js")
    
    # read the quizConfig list
    if (not os.path.isfile(quizconfigpath)):
        errormessage("impossible de trouver le fichier de liste des thèmes (quizConfig.js)")
        return None
    with open(quizconfigpath, 'r') as themefile:
        themestr = themefile.read()
    if (themestr is None):
        errormessage("impossible d'ouvrir le fichier de liste des thèmes (quizConfig.js)")
        return None
    # select only the substring that contains 
    themematch=re.search('{(.|\n)*}', themestr, flags=re.MULTILINE)
    if (themematch is None or len(themematch.groups())<1):
        errormessage("impossible de reconnaitre le contenu du fichier de liste des thèmes (quizConfig.js)")
        return None
    themestr=themematch.group(0)
    # parse json as yaml to work around unquoted keys
    themejson=yaml.load(themestr)
    if (themejson is None or themejson["themeLicence"] is None):
        errormessage("impossible de trouver les thèmes licences dans le contenu du fichier de thèmes (quizConfig.js)")
        return None
    themeraw=themejson["themeLicence"]
    themeindex={}
    # swap the hash table key/value pair and clean the index to reduce data entry error
    for code, text in themeraw.items():
        themeindex[cleantheme(text)]=code
    return themeindex


# Escape characters that have no meaning in XML but are special for TeX / LaTeX
def formulasub(s):
    s=s.group(0)
    if (s is None):
        return ""
    s = re.sub(r'<p .*?>','',s, flags=re.MULTILINE|re.DOTALL)
    s = re.sub('<p>','',s)
    s = re.sub('</p>','',s)
    s = re.sub('<div .*?>','',s, flags=re.MULTILINE|re.DOTALL)
    s = re.sub('<div>','',s)
    s = re.sub('</div>','',s)
    s = re.sub('<br>','',s)
    s = re.sub('<br/>','',s)
    
    s = s.replace('>','&gt;')
    s = s.replace('<','&lt;')
    s = re.sub(r'(&)(?!amp;|gt;|lt;)','&amp;',s)
    return s

# Regex pour gapText
# Le filtre ne traite que des questions avec un seul type de questions
# SHORTANSWER ou MULTICHOICE
def regfilter(s):
    regexgaptextmulti= r"{\d{1,}:(?:MULTICHOICE|MC)(?:|H|_H|V|_V)(?:|S|_S):.*}"
    regexgaptextshort= r"{\d{1,}:(?:SHORTANSWER|SA)(?:|C|_C):.*}"
    regexrightanswer = r"(?:=|%100%)([^#~}]*)"

    multiMatch = re.search(regexgaptextmulti, s)
    shortMatch = re.search(regexgaptextshort, s)

    # Multichoice
    while(multiMatch):
        # Création de l'élément xml réponse
        xmlanswer = XMLsc.textLeaf()
        xmlanswer.attrib["role"] = "gap"
        xmlgapmulti = XMLop.gapM()
        xmloptions = XMLsp.options()

        choices = []
        answerMulti = []

        # 're.search' car un seul match
        rightanswer = re.search(regexrightanswer, multiMatch.group(0))
        answerMulti.append(rightanswer.group(1))

        # Regex pour toutes les réponses
        regexmulti = r"(?:(?:=|%100%|~|:)([a-zA-Z0-9 ,.]*)(?:}|#))"
        # 're.findtier' car plusiseurs choix à récupérer 
        choicesMatch = re.finditer(regexmulti, multiMatch.group(0))

        for matchNum, match in enumerate(choicesMatch):
            choices.append(match.group(1))
            matchNum = matchNum+1

        for i in range(len(choices)):
            option = XMLsp.option()
            option.text = choices[i]
            xmloptions.append(option)

        xmlgapmulti.append(xmloptions)
        xmlgapmulti.tail = answerMulti[0]
        xmlanswer.append(xmlgapmulti)

        s = s.replace(multiMatch.group(0), etree.tostring(xmlanswer, method="html", encoding="unicode"), 1)

        multiMatch = re.search(regexgaptextmulti, s)

    # Shortanswer
    while(shortMatch):
        # Création de l'élément xml réponse
        xmlanswer = XMLsc.textLeaf()
        xmlanswer.attrib["role"] = "gap"

        answerShort = []
        rightShortanswer = re.search(regexrightanswer, shortMatch.group(0))

        answerShort.append(rightShortanswer.group(1))
        xmlanswer.text = answerShort[0]

        s = s.replace(shortMatch.group(0), etree.tostring(xmlanswer, method="html", encoding="unicode"), 1)

        shortMatch = re.search(regexgaptextshort, s)
    
    return s


def cdatasub(s):
    if (s==None):
        return None

    # pour gapText
    s = regfilter(s)
            
    s = re.sub(r'\\\((.*?)\\\)',formulasub,s,flags=re.MULTILINE|re.DOTALL)
    s = re.sub(r'\\\[(.*?)\\\]',formulasub,s,flags=re.MULTILINE|re.DOTALL)
    s = re.sub(r'\$\$(.*?)\$\$',formulasub,s,flags=re.MULTILINE|re.DOTALL)
    s = re.sub(r'(\\\()(.*?)(\\\))',r'<sc:textLeaf role="mathtex">\2</sc:textLeaf>',s,flags=re.MULTILINE|re.DOTALL)
    s = re.sub(r'(\\\[)(.*?)(\\\])',r'<sc:textLeaf role="mathtex">\2</sc:textLeaf>',s,flags=re.MULTILINE|re.DOTALL)
    s = re.sub(r'(\$\$)(.*?)(\$\$)',r'<sc:textLeaf role="mathtex">\2</sc:textLeaf>',s,flags=re.MULTILINE|re.DOTALL)
    return s

# check the image and copy it in the right path
def image(uri, caller):
    return os.path.join('img', newfilename)

# XML tool : return text only if not empty
def getvalidtext(item):
    text=None
    if (item is not None and item.text is not None and len(item.text)>0):
        text=item.text
    return text

# XML tool: return the last child of an element
def lastchild(content):
    if (not content.getchildren()):
        return None
    else:
        return content.getchildren()[-1]

# XML tool: move all children of an element to an other destination element
# must not be nested child
def movexmlcontent(content, destination):
    destination.text=content.text
    content.text=''
    for element in content.iterchildren():
        destination.append(element)
    for element in content.iterchildren():
        element.getparent().remove(element)

# XML tool: remove the element, but keep it's content
def upxmlcontent(element):
    parent=element.getparent()
    if (element.text is not None):
        if (parent.text is not None):
            parent.text+=element.text
        else:
            parent.text=element.text
    after=element
    # move child to next position in parent
    for child in element.iterchildren():
        after.addnext(child)
        after=child
    parent.remove(element)

# XML tool: delete all attributes of an element
def delete_attributes(element):
    for key in element.keys():
        del element.attrib[key]

# write a single image from filename and data
def writeimage(filename, data):
    if (filename is None):
        errormessage("pas de nom de fichier pour l'image")
    elif (data is None):
        errormessage("l'image n'a pas de contenu utilisable")
    else:
        fullimagedir=os.path.join(outputdir, IMAGEDIR)
        if not os.path.exists(fullimagedir):
            os.makedirs(fullimagedir)
        with open(os.path.join(fullimagedir, filename.lower()), "wb") as fh:
            fh.write(base64.decodebytes(bytearray(data.encode())))
            #errormessage("write image file"+os.path.join(fullimagedir, filename))
            fh.close()

# change images that are in an inlined paragraph to a resource block image when possible (nothing else on the same paragraph)
# inlined image are only for tiny icons, so it's best practice to use resources. Moodle does not distinguish between the two.
def image2res(contentxml):
    allimages=contentxml.findall(".//"+nstag('sc', 'inlineImg')+"/[@role='ico']")
    for element in allimages:
        srcpara=element.getparent()
        # if parent paragraph is empty
        if (srcpara.tag==nstag('sc', 'para') and not etree.tostring(srcpara, method="text", encoding='utf-8').decode('utf-8').strip() and len(srcpara)==1):
            newparacontainer=XMLop.txt()
            srcblock=srcpara.getparent().getparent()
            
            # move all text paragraphs following the image in a new text block
            for para in srcpara.itersiblings(preceding=False):
                newparacontainer.append(para)
                #srcpara.remove(para)
            if (len(newparacontainer)>0):
                srcblock.addnext(XMLsp.txt(newparacontainer))
            
            # transform inline para into a new res block
            srcblock.addnext(XMLsp.res(XMLop.resInfoM(), **{nstag('sc', 'refUri'): element.get(nstag('sc', 'refUri'))}))
            srcpara.getparent().remove(srcpara)

# if an html root has content without a first level paragraph, inject this level
# elements are grouped : i.e. if we have 10 consecutive span, 
#
# it only handle simple cases : text, tail or non-paragraph nodes that are attached to the root, gain a paragraph container
# scenari can't have regular content that is not attached to a paragraph.
#
# nodes that contains a sub-paragraph are badly handled : they create a nested paragraph, which is then removed by the content filter
#  (with the side effect of losing some paragraph cuts at some place)
def injectpara(contentxml):
    # paragraph pending to add, where we store elements that are not already in a paragraph
    nextpara=etree.Element("p")
    
    # insertion cursor, last existing paragraph
    cursor=contentxml
    
    # add root text to a paragraph if already existing
    if (contentxml.text is not None and contentxml.text!="" and not contentxml.text.isspace()):
        nextpara.text=contentxml.text
        contentxml.text=None
    
    # add a paragraph level for non-paragraph elements connected directly to the root
    for element in contentxml.iterchildren():
        if (element.tag=='p' or element.tag=='div'):
            # is or contains a paragraph : close current paragraph with any pending text, and prepare a new one
            if (len(nextpara) or nextpara.text):
                if (cursor==contentxml):
                    contentxml.insert(0, nextpara)
                else:
                    cursor.addnext(nextpara)
                nextpara=etree.Element("p")
            cursor=element
        else:
            # is not a paragraph, add to a new paragraph :
            nextpara.append(element)
    if (len(nextpara)>0 or nextpara.text):
        if (cursor==contentxml):
            contentxml.insert(0, nextpara)
        else:
            cursor.addnext(nextpara)
    
    # add a 
    lastelement=lastchild(contentxml)
    if lastelement is not None and lastelement.tail is not None and lastelement.tail!="" and not lastelement.tail.isspace():
        lastelement.text+=" "+lastelement.tail
        lastelement.tail=None
    
    return contentxml

# filter the HTML content of moodle into a Scenari XML paragraph or group of paragraph
# for level, you have :
# contentlvl.FLOWALL = rich opale text with resources... (quiz description and global explaination)
# contentlvl.MULTIPARA = paragraphs without resources (choice label and choice explanation)
# contentlvl.MULTIPARACLOZE = like multipara but in gapText
# contentlvl.PARA = probably not used for MCQ
def filtercontent(content, lvl):
    if (content is None):
        return None
    
    # handle non-HTML content
    # moodle use that for true/false answer
    if (content=='true'):
        content='<p>Vrai</p>'
    elif (content=='false'):
        content='<p>Faux</p>'

    # parse HTML content
    #content=html.unescape(content)
    content=cdatasub(content)
    if (content=="" or content.isspace()):
        return None
    
    content="<root>"+content+"</root>"
    
    try:
        contentxml=etree.fromstring(content,parser)
    except etree.XMLSyntaxError:
        errormessage("some HTML text seems too messed up to be imported: "+content)
        return(XMLop.txt(XMLsc.para(content, **{'space': 'preserve'})))
    
    if (contentxml is None):
        return None
    

    injectpara(contentxml)
    
    # single paragraph fields in Scenari are not allowed multiline in moodle source HTML
    if (lvl==contentlvl.PARA):
        etree.strip_tags(contentxml, "p", "div", "br")
    
    for element in contentxml.iter():
        # only 1 lvl allowed for paragraphs, strip additional levels
        if (element.tag in ["p", "div", "br"]):
            etree.strip_tags(element, "p", "div", "br")
            element.tag=nstag('sc', 'para')
            delete_attributes(element)
            element.set(nstag('xml', 'space'), "preserve")
        elif (element.tag=="sub"):
            delete_attributes(element)
            element.tag=nstag('sc', 'textLeaf')
            element.set("role", "ind")
        elif (element.tag=="sup"):
            delete_attributes(element)
            element.tag=nstag('sc', 'textLeaf')
            element.set("role", "exp")
        elif (element.tag in ["b", "em", "strong", "u"]):
            delete_attributes(element)
            element.tag=nstag('sc', 'inlineStyle')
            element.set("role", "emp")
        elif (element.tag=="ul"):
            delete_attributes(element)
            element.tag=nstag('sc', 'itemizedList')
        elif (element.tag=="ol"):
            delete_attributes(element)
            element.tag=nstag('sc', 'orderedList')
        elif (element.tag=="li"):
            element.tag=nstag('sc', 'listItem')
            delete_attributes(element)
            xmlpara=XMLsc.para()
            xmlpara.set(nstag('xml', 'space'), "preserve")
            movexmlcontent(element, xmlpara)
            element.append(xmlpara)
        elif (element.tag=="a"):
            href=element.get("href")
            if (href is None):
                errormessage("URL invalide pour le lien web : "+etree.tostring(xmlroot, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8"))
                href=""
            title=element.get("title")
            text=element.text
            element.tag=nstag('sc', 'phrase')
            delete_attributes(element)
            element.set("role", "url")
            element.text=""
            if (title and title!=text and title!=href):
                element.insert(0, XMLop.urlM(XMLsp.url(href), XMLsp.title(title)))
            else:
                element.insert(0, XMLop.urlM(XMLsp.url(href)))
            lastchild(element).tail=text
        elif (element.tag=="img"):
            # fetch content of original img tag
            src=element.get("src")
            if (src.startswith("@@PLUGINFILE@@/")):
                filename=src.split("@@PLUGINFILE@@/",1)[1]
            elif (src.startswith("data:image/")):
                if (src.lower().startswith("data:image/png;base64,")):
                    ext=".png"
                elif (src.lower().startswith("data:image/jpg;base64,")):
                    ext=".jpg"
                elif (src.lower().startswith("data:image/gif;base64,")):
                    ext=".gif"
                else:
                    ext=None
                if ext is None:
                    errormessage("impossible de traiter l'image: format de fichier non reconnu")
                    filename="IMAGE_FORMAT_ERROR.png"
                else:
                    filename=generateId(imgcount, "img")+ext
                    imgcount+=1
                    writeimage(filename, src.split(";base64,",1)[1])

            else:
                filename="IMAGE_DATA_ERROR.png"
                errormessage("impossible de traiter l'image: type de donnée non reconnue")
            
            delete_attributes(element)
            element.tag=nstag('sc', 'inlineImg')
            element.set("role", "ico")
            element.set(nstag('sc', 'refUri'), IMAGEDIR+"/"+urllib.parse.unquote(filename).lower())

        else:
            if (not (element.tag[0]=='{' or element.tag in ['root', 'sc:textLeaf'])):
                if element.tag not in ['span']:
                    errormessage("tag inconnu: "+element.tag+", contenu: "+etree.tostring(element, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8"))
                # remove the element but keep it's content, if the element is not supported by Scenari
                upxmlcontent(element)
    
    # put the xml content in the right envelope
    # it depends on what Scenari expects for this field
    if (lvl==contentlvl.FLOWALL):
        inserable=XMLop.txt()
        movexmlcontent(contentxml, inserable)
        contentxml=XMLop.res(XMLsp.txt(inserable))
        image2res(contentxml)
    elif (lvl==contentlvl.MULTIPARA):
        inserable=XMLop.txt()
        movexmlcontent(contentxml, inserable)
        contentxml=inserable
    elif (lvl==contentlvl.MULTIPARACLOZE):
        inserable=XMLop.clozeTxt()
        movexmlcontent(contentxml, inserable)
        contentxml=inserable
    elif (lvl==contentlvl.PARA):
        inserable=XMLsc.para()
        movexmlcontent(contentxml, inserable)
        contentxml=XMLop.txt(inserable)
        
    return contentxml

def writeallfiles(filetags):
    for element in filetags:
        # fetch content of original img tag
        filename=element.get("name")
        writeimage(filename, element.text)


# parse MCQ or SCQ
def parsemultichoice(item):
    global debugtitle
    
    debugtitle=title=getvalidtext(item.find('.//name/text'))
    xmlmeta=filtermeta(item)
    
    question=filtercontent(item.findtext('.//questiontext/text'), contentlvl.FLOWALL)
    writeallfiles(item.findall('.//questiontext/file'))
    xmlquestion=None
    if (question is not None):
        xmlquestion=XMLsc.question(question)
    
    globalexplanation=filtercontent(item.findtext('.//generalfeedback/text'), contentlvl.FLOWALL)
    writeallfiles(item.findall('.//generalfeedback/file'))
    xmlglobalexplanation=None
    if (globalexplanation is not None):
        xmlglobalexplanation=XMLsc.globalExplanation(globalexplanation)
    
    single=(item.findtext('.//single')=='true' or item.get('type')=='truefalse')
    
    lastsolution=None
    solutioncount=0
    i=1
    
    # handle choice... choices contains a label and optionally an explanation
    xmlchoices=XMLsc.choices()
    
    for answer in item.findall('.//answer'):
        try:
            answersolution=(float(answer.get('fraction'))>0)
        except ValueError:
            errormessage("la note d'un des choix pour l'exercice \""+title+"\" n'est pas précisé")
            answersolution=false
        
        if (answersolution):
            lastsolution=i
            solutioncount+=1
        
        choice=filtercontent(answer.findtext("./text"), contentlvl.MULTIPARA)
        if (choice is not None):
            xmlchoice=XMLsc.choice(XMLsc.choiceLabel(choice))
        else:
            errormessage("choix vide : "+etree.tostring(answer, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8"))
        if (not single):
            xmlchoice.attrib['solution']='checked' if answersolution else 'unchecked'
        choiceexplanation=filtercontent(answer.findtext("./feedback/text"), contentlvl.MULTIPARA)
        if (choiceexplanation is not None):
            xmlchoice.append(XMLsc.choiceExplanation(choiceexplanation))
        
        xmlchoices.append(xmlchoice)
        i+=1
    
    question=filtercontent(item.findtext('.//questiontext/text'), contentlvl.FLOWALL)
    if (question is not None):
        xmlquestion=XMLsc.question(question)
    
    xmlsolution=None
    if (solutioncount<1):
            errormessage("L'exercice n'a aucun choix coché")
    if (single):
        if (solutioncount>1):
            errormessage("L'exercice est à réponse unique mais 2 choix sont cochés")
        xmlquiz=XMLop.mcqSur()
        if (lastsolution is not None):
            xmlsolution=XMLsc.solution(choice=str(lastsolution))
    else:
        xmlquiz=XMLop.mcqMur()
    
    for element in [xmlmeta, xmlquestion, xmlchoices, xmlsolution, xmlglobalexplanation]:
        if (element is not None):
            xmlquiz.append(element)
    
    return xmlquiz


# parse a single question
# mostly check if it's the right type, and call a more specific handler
# also write the data in the quiz file
def parsequestion(item):
    global xmlcount, debugtitle
    itemtype=item.get('type')
    
    if (itemtype in ['multichoice', 'truefalse']):
        xmlquestion=parsemultichoice(item)
        
    elif (itemtype=='cloze'):
        xmlquestion=parsegapfill(item)

    elif (itemtype=='category'):
        return
    else:
        errormessage("type d'exercice non supporté rencontré : "+itemtype)
        return
    
    xmlroot=XMLsc.item(xmlquestion)
    
    if (options.prefix):
        questionfile = options.prefix +"-"+ str(xmlcount).zfill(3)+".quiz"
    else:
        questionfile = generateId(xmlcount, debugtitle)+".quiz"
    xmlcount+=1
    
    #sys.stdout.write(etree.tostring(xmlroot, xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8"))
    etree.ElementTree(xmlroot).write(os.path.join(outputdir,questionfile), xml_declaration=True, encoding='utf-8', pretty_print=True)

def parsegapfill(item):
    
    xmlmeta = filtermeta(item)

    question=filtercontent(item.findtext('.//questiontext/text'), contentlvl.MULTIPARACLOZE)

    xmlgaptext = XMLsc.gapText()
    xmlgaptext.append(question)

    generalfeedback = filtercontent(item.findtext('.//generalfeedback/text'), contentlvl.FLOWALL)
    if (generalfeedback is not None):
        xmlglobalexplanation = XMLsc.globalExplanation(generalfeedback)
    else:
        xmlglobalexplanation = XMLsc.globalExplanation()

    # Root de la question
    xmlcloze = XMLopsp.cloze()

    for child in [xmlmeta, xmlgaptext, xmlglobalexplanation]:
        if (child is not None):
            xmlcloze.append(child)

    return xmlcloze
	
def filtermeta(item):
    title = getvalidtext(item.find('.//name/text'))
    xmlmeta=None
    
    themes=[]
    complexite=None
    niveaux=[]
    origine=None
    keywords=[]
    
    for element in item.findall('.//tags/tag/text'):
        tag=element.text
        if (useless(tag)):
            continue
        try:
            tagname, value=tag.split(":",1)
        except ValueError:
            tagname=tag
            value=""

        tagname=tagname.lower().strip()
        value=value.strip()

        xmlmeta=XMLop.exeM()
        if tagname.startswith('thème') or tagname.startswith('theme'):
            # check if we have something that match with the theme text, and change it to a key
            if (themeindex):
                cleanvalue=cleantheme(value)
                if (themeindex[cleantheme(value)] is not None):
                    value=themeindex[cleanvalue]
            themes.append(value)
        elif tagname.startswith('complexité') or tagname.startswith('complexite'):
            complexite=value
            try:
                if (int(complexite)<1 or int(complexite)>4):
                    errormessage("niveau de complexité bloom invalide: "+value+", devrait être [1..4]")
            except ValueError:
                errormessage("niveau de complexité bloom invalide: "+value+", devrait être [1..4]")
        elif tagname.startswith('niveau'):
            value=value.upper()
            if (value not in ['L0', 'L1', 'L2', 'L3', 'M1', 'M2']):
                errormessage("niveau d'étude invalide: "+value+", devrait être [L0, L1, L2, L3, M1, M2]")
            niveaux.append(value)
        elif tagname.startswith('origine'):
            origine=value
        else:
            keywords.append(tag)

    # Remplissage du op:exeM
    if (title or themes or complexite or niveaux or origine or keywords):
        xmlmeta=XMLop.exeM()
        if title:
            xmlmeta.append(XMLsp.title(title))
        for singletheme in themes:
            xmlmeta.append(XMLsp.themeLicence(singletheme))
        if complexite:
            xmlmeta.append(XMLsp.level(complexite))
        for singleniveau in niveaux:
            xmlmeta.append(XMLsp.educationLevel(singleniveau))
        if (origine or keywords):
            xmlmetainfo=XMLop.info()
            if (keywords):
                xmlmetakeywords=XMLop.keywds()
                for singlekeyword in keywords:
                    xmlmetakeywords.append(XMLsp.keywd(singlekeyword))
                xmlmetainfo.append(XMLsp.keywds(xmlmetakeywords))
            if (origine):
                xmlmetainfo.append(XMLsp.cpyrgt(XMLop.sPara(XMLsc.para(origine))))
            xmlmeta.append(XMLsp.info(xmlmetainfo))

    return xmlmeta

def parsemoodle(intputfile):
    tree = etree.parse(inputfile, parser)
    for item in tree.findall('./question'):
        parsequestion(item)


themeindex=loadthemes()
parsemoodle(inputfile)


#print(etree.tostring(injectpara(etree.fromstring("<root>a ! <b>b</b> <p>c</b><p>d<span>e</span>f</p>\n <sup>f'</sup> <p>g</p>h</root>",parser)), xml_declaration=True, encoding='utf-8', pretty_print=True).decode("utf-8"))


