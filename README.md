# moodle2scenari

Scripts to convert Moodle XML MCQ files Scenari files (opale, topoze / faq2sciences).

## Package content 

/ : pyhon conversion scripts
/web/ : web launcher for the script


## Basic install

The script may be tweaked to run on other OS but it has only be tested on Linux :

    cd /opt
    git clone "https://framagit.org/stephanep/moodle2scenari" moodle2scenari
    apt-get install python3-lxml

Other package may be required


## Basic usage

usage: moodle2scenari.py [-h] inputfile outputdir [prefix]

Convert from Mooodle XML to topoze / opale

positional arguments:
  inputfile   input moodle XML file (.xml)
  outputdir   output directory
  prefix      (optionnal) prefix for generated quizz name

Example :

	./moodle2opale.py mycontent.xml out

With "mycontent" the folder containing the moodle XML and "out" the name of the directory
that will be created to store the output. 


Output will be generated in the "out" directory, created in the current directory. Script will stop
if it already exists to avoid important content beeing overwriten.


## Web frontend

It's a simple php script

Make sure you extract the script outside of the webroot folder (i.e. /opt/moodle2scenari/ )
and then link only the web folder to your webroot (i.e. : ln -s /opt/moodle2scenari/web /var/www/moodle2scenari )

Create a new writable directory :

    mkdir /opt/moodle2scenari/web/upload
    chown www-data.www-data /opt/moodle2scenari/web/upload


Create a cron cleanup to remove old files in /tmp/upload and /opt/moodle2scenari/web/upload to avoid keeping
user data too long.

chroot should be used to improve security but has not been integrated to the webpage, so it's provided without
any security waranty.

